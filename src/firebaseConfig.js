import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore/lite"

const firebaseConfig = {
  apiKey: "AIzaSyCFF3Hbg7e31OofIh-tFDgETA2Dhqhx-bI",
  authDomain: "vue-3-2023-cf695.firebaseapp.com",
  projectId: "vue-3-2023-cf695",
  storageBucket: "vue-3-2023-cf695.appspot.com",
  messagingSenderId: "530887222085",
  appId: "1:530887222085:web:d49379f6894e54cbbd142c"
};

initializeApp(firebaseConfig);
const auth = getAuth()
const db = getFirestore()

export { auth, db };