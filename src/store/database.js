import { defineStore } from "pinia"
import { collection, getDocs, query, where, addDoc, doc, deleteDoc, getDoc, updateDoc } from "firebase/firestore/lite"
import { db, auth } from "@/firebaseConfig.js"
import { nanoid } from "nanoid"

export const useDatabaseStore = defineStore("database", {
    state: () => ({
        documents: [],
        loadingDoc: false
    }),
    actions: {
        async getUrls() {
            try {
                if (this.documents.length !== 0) {
                    return
                }
                this.loadingDoc = true
                const q = query(collection(db, "urls"), where("user", "==", auth.currentUser.uid))
                const querySnapshot = await getDocs(q)
                querySnapshot.forEach(doc => {
                    this.documents.push({
                        id: doc.id,
                        ...doc.data()
                    })
                })
            } catch (e) {
                console.log(e)
            } finally {
                this.loadingDoc = false
            }
        },
        async addUrl(name) {
            try {
                const objetoDoc = {
                    name: name,
                    short: nanoid(6),
                    user: auth.currentUser.uid
                }
                const docRef = await addDoc(collection(db, "urls"), objetoDoc)
                this.documents.push({
                    ...objetoDoc,
                    id: docRef.id
                })
            } catch (e) {
                console.log(e)
            } finally {

            }
        },
        async readUrl(id) {
            try {
                const docRef = doc(db, "urls", id)
                const docSnap = await getDoc(docRef)

                if (!docSnap.exists()) {
                    throw new Error("No existe el documento")
                }
                if (docSnap.data().user !== auth.currentUser.uid) {
                    throw new Error("Este documento no le pertenece")
                }

                return docSnap.data().name
            } catch (e) {
                console.log(e)
            } finally {

            }
        },
        async updateUrl(id, name) {
            try {
                const docRef = doc(db, "urls", id)
                const docSnap = await getDoc(docRef)

                if (!docSnap.exists()) {
                    throw new Error("No existe el documento")
                }
                if (docSnap.data().user !== auth.currentUser.uid) {
                    throw new Error("Este documento no le pertenece")
                }
                await updateDoc(docRef, {
                    name: name
                })
                this.documents = this.documents.map(item => item.id === id ? ({ ...item, name: name }) : item)
            } catch (e) {
                console.log(e)
            } finally {

            }
        },
        async deleteUrl(id) {
            try {
                const docRef = doc(db, "urls", id)
                const docSnap = await getDoc(docRef)
                if (!docSnap.exists()) {
                    throw new Error("No existe el documento")
                }
                if (docSnap.data().user !== auth.currentUser.uid) {
                    throw new Error("Este documento no le pertenece")
                }
                await deleteDoc(docRef)
                this.documents = this.documents.filter(item => item.id !== id)
            } catch (e) {
                console.log(e)
            } finally {

            }
        }
    }
})