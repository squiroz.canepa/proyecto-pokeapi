import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, onAuthStateChanged } from 'firebase/auth'
import { defineStore } from 'pinia'
import { auth } from '../firebaseConfig'
import router from '../router'
import { useDatabaseStore } from "./database"

export const useUserStore = defineStore('user', {
    state: () => ({
        userData: null,
        loadingUser: false
    }),
    actions: {
        async registerUser(email, password) {
            this.loadingUser = true
            try {
                const { user } = await createUserWithEmailAndPassword(auth, email, password)
                this.userData = {email: user.email, password: user.password}
                router.push("/")
            } catch (e) {
                console.log(e)
            } finally {
                this.loadingUser = false
            }
        },
        async loginUser(email, password) {
            this.loadingUser = true
            try{
                const {user} = await signInWithEmailAndPassword(auth, email, password)
                this.userData = {email: user.email, password: user.password}
                router.push("/")
            } catch(e){
                console.log(e)
            } finally {
                this.loadingUser = false
            }
        },
        async logoutUser(){
            const databaseStore = useDatabaseStore()
            databaseStore.$reset()
            try {
                await signOut(auth)
                this.userData = null
                router.push("/login")
            }catch(e){
                console.log(e)
            }
        },
        currentUser() {
            return new Promise((resolve, reject) => {
                const unsuscribe = onAuthStateChanged(auth, user => {
                    if(user){
                        this.userData = {email: user.email, password: user.password}
                    }else{
                        this.user = null
                        const databaseStore = useDatabaseStore()
                        databaseStore.$reset()
                    }
                    resolve(user)
                },e => reject(e))
                unsuscribe()
            })
        }
    },
})